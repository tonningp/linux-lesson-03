# Learning Objectives

By the end of this section, you should be able to:

*Identify Linux filesystems.
*Identify the differences between partitions and filesystems.
*Describe the boot process.
*Install Linux on a computer.



The Linux boot process is the procedure for initializing the system. It consists of everything that happens from when the computer power is first switched on until the user interface is fully operational.

Having a good understanding of the steps in the boot process may help you with troubleshooting problems, as well as with tailoring the computer's performance to your needs.

On the other hand, the boot process can be rather technical, and you can start using Linux without knowing all the details.

NOTE: You may want to come back and study this section later, if you want to first get a good feel for how to use a Linux system.


![Alt Text](https://cis.vvc.edu/tonning/dirtree.jpg)
![Alt Text](https://cis.vvc.edu/tonning/chapter03_flowchart_scr15_1.jpg)

